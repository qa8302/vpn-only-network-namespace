# VPN-only Network Namespace

Linux Network Namespaces can be used to create a namespace for running
applications which are only allowed to use certain network connections.
This is quite practical every time it is critical that an application use
a VPN connection exclusively, when the uplink is managed by network managers
such as NetworkManager.

Usage is simple:

    sudo ./setup-vpn-netns.sh <vpn-interface> <nameserver>

For example:

    sudo ./setup-vpn-netns.sh tun0 10.8.10.1

The nameserver is of course specific to the VPN connection. Alternatively,
public DNS servers such as 8.8.8.8 can also be used, exposing DNS queries
to the operators of those servers instead.

## Impact

This script will modify some system settings for you, which will have some
impact. In particular, it will:

- **Enable IP forwarding**
- **Flush some iptables tables and add some rules**
- Create a network namespace
- Create virtual interfaces
